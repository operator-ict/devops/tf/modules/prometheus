# Unreleased
## Added
## Changed
## Fixed

# [0.1.0] - 2023-09-18
## Changed
- Update azurerm version to 4.2.0+

# [0.0.2] - 2023-08-16
## Changed
- Update azurerm version

# [0.0.1] - 2023-08-16
## Added
- Initial release
